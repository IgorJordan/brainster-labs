document.querySelector("#marketing").addEventListener('change', handle1);
document.querySelector("#programming").addEventListener("change", handle2);
document.querySelector("#design").addEventListener("change", handle3);


function handle1() {
    hideAll();
            
    if(document.querySelector("#marketing").checked == false
        && document.querySelector("#programming").checked == false
        && document.querySelector("#design").checked == false) {
        showSome(".cards");
        document.querySelector(".btn-mkg").style.cssText = "color: white; background-color: #333333";
    }
    
    if(document.querySelector("#marketing").checked) {
        document.querySelector("#programming").checked = false;
        document.querySelector("#design").checked = false;
        showSome(".marketing");
        document.querySelector(".btn-mkg").style.cssText = "color: #333333; background-color: #ff0034";
        document.querySelector(".btn-it").style.cssText = "color: white; background-color: #333333";
        document.querySelector(".btn-dsg").style.cssText = "color: white; background-color: #333333";
    }
    if(document.querySelector("#programming").checked) {
        showSome(".programming");
    }
    if(document.querySelector("#design").checked) {
        showSome(".design");
    }
}
    
function handle2() {
    hideAll();
            
    if(document.querySelector("#marketing").checked == false
        && document.querySelector("#programming").checked == false
        && document.querySelector("#design").checked == false) {
        showSome(".cards");
        document.querySelector(".btn-it").style.cssText = "color: white; background-color: #333333";
    }
    
    if(document.querySelector("#programming").checked) {
        document.querySelector("#marketing").checked = false;
        document.querySelector("#design").checked = false;
        showSome(".programming");
        document.querySelector(".btn-it").style.cssText = "color: #333333; background-color: #ff0034";
        document.querySelector(".btn-dsg").style.cssText = "color: white; background-color: #333333";
        document.querySelector(".btn-mkg").style.cssText = "color: white; background-color: #333333";
    }
    if(document.querySelector("#marketing").checked) {
        showSome(".marketing");
    }
    if(document.querySelector("#design").checked) {
        showSome(".design");
    }
}
            
function handle3() {
    hideAll();
            
    if(document.querySelector("#marketing").checked == false
        && document.querySelector("#programming").checked == false
        && document.querySelector("#design").checked == false) {
        showSome(".cards");
        document.querySelector(".btn-dsg").style.cssText = "color: white; background-color: #333333";
    }

    if(document.querySelector("#design").checked) {
        document.querySelector("#marketing").checked = false;
        document.querySelector("#programming").checked = false;
        showSome(".design");
        document.querySelector(".btn-dsg").style.cssText = "color: #333333; background-color: #ff0034";
        document.querySelector(".btn-it").style.cssText = "color: white; background-color: #333333";
        document.querySelector(".btn-mkg").style.cssText = "color: white; background-color: #333333";
    }
    if(document.querySelector("#programming").checked) {   
        showSome(".programming");
    }
    if(document.querySelector("#marketing").checked) {
        showSome(".marketing");
    }
            
}

function hideAll() {
    var allCards = document.querySelectorAll(".cards");
    var i = 0;
    while(i < allCards.length) {
        allCards[i].style.display = 'none';
        i++;
    }
}

function showSome(type) {
    var elements = document.querySelectorAll(type);
    i = 0;
    while(i < elements.length) {
        elements[i].style.display = "block";
        i++;
    }
}

/**
 * create load more button
 */

visibleCarts = 6;

function filterCarts(category) {
    carts = document.querySelectorAll('.cards');
    for (i = 0; i < carts.length; i++) {
        carts[i].style.display = "none";
    }

    showCarts = document.querySelectorAll(category);
    for (i = 0; i < showCarts.length; i++) {
        if (i < visibleCarts)
            showCarts[i].style.display = "inline-block";
        else
            break;
    }
}

function loadMore() {
    end = visibleCarts + 6;
    carts = document.querySelectorAll('.cards');
    for (i = visibleCarts; i < end; i++) {
        if (i < 20)
            carts[i].style.display = "inline-block";
        else {
            document.querySelector('#loadmorebtn').style.display = 'none';
            break;
        }
    }

    visibleCarts = end;
}

/**
 * curtain menu
 */

function openNav() {
    document.getElementById("myNav").style.width = "100%";
}
  
  function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}