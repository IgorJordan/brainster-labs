console.log('Loaded');


let customSelect = document.querySelector('.custom-select');
let selectedValue = document.querySelector('.selected-value');
let allOptions = document.querySelectorAll('.option')

customSelect.addEventListener('click', function () {
    customSelect.querySelector('.options').classList.toggle('open');
});

window.addEventListener('click', function (e) {
    if (!customSelect.contains(e.target)) {
        customSelect.querySelector('.options').classList.remove('open');
    }
});

for (let i = 0; i < allOptions.length; i++) {
    const option = allOptions[i];
    option.addEventListener('click', function () {
        if (!option.classList.contains('selected')) {
            let findSelected = option.parentNode.querySelector('.option.selected');
            if (findSelected) {
                findSelected.classList.remove('selected');
            }
            option.classList.add('selected');
            selectedValue.innerHTML = option.textContent;
        }
    })
}